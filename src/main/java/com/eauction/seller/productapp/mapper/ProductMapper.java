package com.eauction.seller.productapp.mapper;

import com.eauction.seller.productapp.dto.Bid;
import com.eauction.seller.productapp.dto.ProductBid;
import com.eauction.seller.productapp.dto.ProductDTO;
import com.eauction.seller.productapp.dto.ProductDetailsDTO;
import com.eauction.seller.productapp.entity.BuyerBidDetail;
import com.eauction.seller.productapp.entity.ProductDetail;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    ProductDetail fromProductDetailsDTO(ProductDetailsDTO productDetailsDTO);

    List<ProductDTO> fromProductDetails(List<ProductDetail> productDetails);

    ProductBid fromProductDetails(ProductDetail productDetail);

    List<Bid> fromBidDetails(List<BuyerBidDetail> buyerBidDetails);

}