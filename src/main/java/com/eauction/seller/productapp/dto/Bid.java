package com.eauction.seller.productapp.dto;

import lombok.Data;

@Data
public class Bid {
    private String bidAmount;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
}