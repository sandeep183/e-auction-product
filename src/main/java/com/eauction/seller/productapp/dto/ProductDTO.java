package com.eauction.seller.productapp.dto;

import lombok.Data;

@Data
public class ProductDTO {
    private String productId;
    private String productName;
}
