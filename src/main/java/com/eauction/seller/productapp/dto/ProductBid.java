package com.eauction.seller.productapp.dto;

import lombok.Data;

import java.util.List;

@Data
public class ProductBid {
    private String productId;
    private String productName;
    private String shortDescription;
    private String detailedDescription;
    private String category;
    private String startingPrice;
    private String bidEndDate;
    private List<Bid> bids;
}