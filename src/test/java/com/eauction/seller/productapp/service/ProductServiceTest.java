package com.eauction.seller.productapp.service;

import com.eauction.seller.productapp.dto.Bid;
import com.eauction.seller.productapp.dto.ProductBid;
import com.eauction.seller.productapp.dto.ProductDTO;
import com.eauction.seller.productapp.dto.ProductDetailsDTO;
import com.eauction.seller.productapp.entity.BuyerBidDetail;
import com.eauction.seller.productapp.entity.ProductDetail;
import com.eauction.seller.productapp.mapper.ProductMapper;
import com.eauction.seller.productapp.repository.BidDetailsRepository;
import com.eauction.seller.productapp.repository.ProductDetailsRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(MockitoExtension.class)
class ProductServiceTest {
    @Mock
    private ProductDetailsRepository productDetailsRepository;
    @Mock
    private BidDetailsRepository bidDetailsRepository;
    @Mock
    private ProductMapper productMapper;
    @InjectMocks
    private ProductService productService;


    @Test
    void testAddNewProduct() {
        ProductDetailsDTO productDetailsDTO = populateProductDetailsDTO();
        Mockito.when(productMapper.fromProductDetailsDTO(productDetailsDTO)).thenReturn(getProductDetail());
        ProductDetail productDetail = productService.addNewProduct(productDetailsDTO);
        assertEquals("Paint Brush", productDetail.getProductName());
    }

    @Test
    void testAddProductNullInput() {
        ProductDetail productDetail = productService.addNewProduct(null);
        assertNull(productDetail);
    }

    @Test
    void testFetchProducts() {
        List<ProductDetail> productDetails = Arrays.asList(getProductDetail());
        Mockito.when(productDetailsRepository.fetchAllProducts()).thenReturn(productDetails);
        Mockito.when(productMapper.fromProductDetails(productDetails)).thenReturn(getProductDTOs());
        List<ProductDTO> productDTOS = productService.fetchProducts();
        assertEquals("Paint Brush", productDTOS.get(0).getProductName());
    }

    @Test
    void testFetchProductBids() {
        Mockito.when(productDetailsRepository.fetchProductById("1111-2222")).thenReturn(getProductDetail());
        Mockito.when(bidDetailsRepository.fetchBids("1111-2222")).thenReturn(getBuyerBidDetails());
        Mockito.when(productMapper.fromProductDetails(getProductDetail())).thenReturn(new ProductBid());
        Mockito.when(productMapper.fromBidDetails(getBuyerBidDetails())).thenReturn(populateBids());
        ProductBid productBid = productService.fetchProductBids("1111-2222");
        assertEquals("3000", productBid.getBids().get(0).getBidAmount());
    }

    private ProductDetailsDTO populateProductDetailsDTO() {
        ProductDetailsDTO productDetailsDTO = new ProductDetailsDTO();
        productDetailsDTO.setProductName("Paint Brush");
        productDetailsDTO.setCategory("Painting");
        productDetailsDTO.setShortDescription("Paint Brush");
        productDetailsDTO.setDetailedDescription("It is a branded paint brush");
        productDetailsDTO.setBidEndDate("12/12/2022");
        productDetailsDTO.setStartingPrice(300);
        productDetailsDTO.setFirstName("Bravin");
        productDetailsDTO.setLastName("Ninja");
        productDetailsDTO.setAddress("S1, Vivekananda Nagar, Sithalapakkam");
        productDetailsDTO.setCity("Chennai");
        productDetailsDTO.setEmail("bravin@gmail.com");
        productDetailsDTO.setPhone(1234567890);
        productDetailsDTO.setPin("230000");
        productDetailsDTO.setState("TN");
        return productDetailsDTO;
    }

    private ProductDetail getProductDetail() {
        ProductDetail productDetail = new ProductDetail();
        productDetail.setProductName("Paint Brush");
        productDetail.setCategory("Painting");
        productDetail.setShortDescription("Paint Brush");
        productDetail.setDetailedDescription("It is a branded paint brush");
        productDetail.setBidEndDate("12/12/2022");
        productDetail.setStartingPrice(300);
        productDetail.setFirstName("Bravin");
        productDetail.setLastName("Ninja");
        productDetail.setAddress("S1, Vivekananda Nagar, Sithalapakkam");
        productDetail.setCity("Chennai");
        productDetail.setEmail("bravin@gmail.com");
        productDetail.setPhone(1234567890);
        productDetail.setPin("230000");
        productDetail.setState("TN");
        return productDetail;
    }

    private List<BuyerBidDetail> getBuyerBidDetails() {
        List<BuyerBidDetail> buyerBidDetails = new ArrayList<>();
        BuyerBidDetail buyerBidDetail = new BuyerBidDetail();
        buyerBidDetail.setProductId("1111-2222");
        buyerBidDetail.setBidAmount("3000");
        buyerBidDetail.setEmail("newbuyer@gmail.com");
        buyerBidDetails.add(buyerBidDetail);
        return buyerBidDetails;
    }

    private List<Bid> populateBids() {
        List<Bid> bids = new ArrayList<>();
        Bid bid = new Bid();
        bid.setBidAmount("3000");
        bid.setEmail("newbuyer@gmail.com");
        bids.add(bid);
        return bids;
    }

    private List<ProductDTO> getProductDTOs() {
        List<ProductDTO> productDTOS = new ArrayList<>();
        ProductDTO productDTO = new ProductDTO();
        productDTO.setProductId("1111-2222");
        productDTO.setProductName("Paint Brush");
        productDTOS.add(productDTO);
        return productDTOS;
    }


}