package com.eauction.seller.productapp.validation;

import com.eauction.seller.productapp.dto.ProductDetailsDTO;
import com.eauction.seller.productapp.exception.InvalidProductInputsException;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.utils.StringUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

@Service
public class ValidationService {
    private final List<String> CATEGORY = Arrays.asList("Painting", "Sculptor", "Ornament");
    public void validateProductInputs(ProductDetailsDTO productDetailsDTO) {
        String errorMessage = "";
        if(!validateCategory(productDetailsDTO.getCategory())) {
            errorMessage = "Category value is invalid. ";
        }
        if(!validateBidEndDate(productDetailsDTO.getBidEndDate())) {
            errorMessage = errorMessage + "Bid End date should be a future date.";
        }

        if(!StringUtils.isEmpty(errorMessage)) {
            throw new InvalidProductInputsException(errorMessage);
        }

    }

    private boolean validateCategory(String category) {
        return !StringUtils.isEmpty(category) && CATEGORY.contains(category);
    }

    private boolean validateBidEndDate(String bidEndDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate localDate = LocalDate.parse(bidEndDate,formatter);
        return localDate.isAfter(LocalDate.now());
    }
}